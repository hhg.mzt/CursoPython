from typing import TypeVar, Generic, List

T = TypeVar('T')

class Stack(Generic[T]):
    def __init__(self):
        self.items: List[T] = []

    def push(self, item: T):
        self.items.append(item)

    def pop(self) -> T:
        return self.items.pop()
    

# Crear una instancia de Stack para enteros
stack_enteros = Stack[int]()

# Agregar elementos a la pila
stack_enteros.push(1)
stack_enteros.push(2)
stack_enteros.push(3)

# Eliminar elementos de la pila
print(stack_enteros.pop())  # Output: 3
print(stack_enteros.pop())  # Output: 2
print(stack_enteros.pop())  # Output: 1

# Crear una instancia de Stack para cadenas
stack_cadenas = Stack[str]()

# Agregar elementos a la pila
stack_cadenas.push("Hola")
stack_cadenas.push("Mundo")

# Eliminar elementos de la pila
print(stack_cadenas.pop())  # Output: "Mundo"
print(stack_cadenas.pop())  # Output: "Hola"

def first(container: List[T]) -> T:
    return container[0]

list_one: List[str] = ["a", "b", "c"]
print(first(list_one))
    
list_two: List[int] = [1, 2, 3]
print(first(list_two))
import datetime
from enum import Enum


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3


class WebMethod(str, Enum):
    GET = "get"
    POST = "post"
    PUT = "put"
    DELTE = "delete"


web_methods = {"GET": "get", "POST": "post", "PUT": "put", "DELETE": "delete"}

months = ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre')

months_dicc = { 1: 'Enero', 2: 'Febrero', 3: 'Marzo',
                4: 'Abril', 5: 'Mayo', 6: 'Junio',
                7: 'Julio', 8: 'Agosto', 9: 'Septiembre', 
                10: 'Octubre',  11: 'Noviembre', 12: 'Diciembre' }


current_date = datetime.datetime.now()
print(current_date)
print(type(current_date))

def formatDate(date: datetime.datetime)->str:
    #return f"{date.day}/{months[date.month - 1]}/{date.year}"
    return f"{date.day}/{months_dicc[date.month]}/{date.year}"

print(formatDate(current_date))
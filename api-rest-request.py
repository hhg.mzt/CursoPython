### En este ejemplo consumis un api de chuck norris
### Y para escalar o tratar temas de OOP el retorno del response lo haremos 
### usando una clase, que abstraiga la response de la API
from typing import TypeVar, Generic, List
import requests

API_CHUCK = "https://api.chucknorris.io"

class ResponseApiChuckJoke:
    categories: List[str]
    created_at: str
    icon_url: str
    id: str
    updated_at: str
    url: str
    value: str

    def __init__(self, categories: List[str], created_at: str, icon_url: str, id: str, updated_at: str, url: str, value: str):
        self.categories = categories
        self.created_at = created_at
        self.icon_url = icon_url
        self.id = id
        self.updated_at = updated_at
        self.url = url
        self.value = value

    def to_json(self):
        return {
            'categories': self.categories,
            'created_at': self.created_at,
            'icon_url': self.icon_url,
            'id': self.id,
            'updated_at': self.updated_at,
            'url': self.url,
            'value': self.value
        }

T = TypeVar('T')

class ResponseApiChuckJokeQuery(Generic[T]):
    total: int
    result: List[T]

    def __init__(self, total: int, result: List[T]) -> None:
        self.total = total
        self.result = result


def consume_get(url: str) -> ResponseApiChuckJoke:
    response = requests.get(url)
    return ResponseApiChuckJoke(**response.json())

## se podría haber usado herencia para hacerlo mas simple pero habia que prácticar con génericos
def consume_get_query(url: str) -> ResponseApiChuckJokeQuery[ResponseApiChuckJoke]:
    response = requests.get(url)
    data = response.json()
    return ResponseApiChuckJokeQuery[ResponseApiChuckJoke](total=data["total"], result= [item for item in data['result']])
    


response = consume_get(f"{API_CHUCK}/jokes/random")
print(response.to_json())

response_query = consume_get_query(f"{API_CHUCK}/jokes/search?query=dev")
print(response_query.total)
print(response_query.result)

print("##"*12)
## haciendo uso de programación funcional (fiter) vamos a busca una categoría de chistes
chistes_movie = list(filter(lambda x: 'movie' in x["categories"], response_query.result))

# Imprimir los resultados
print(f"Se encontraron {len(chistes_movie)} chistes en la categoría 'movie':")
for chiste in chistes_movie:
    print(f"{chiste['value']}")